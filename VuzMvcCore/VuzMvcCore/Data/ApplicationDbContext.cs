﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VuzMvcCore.Models;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Faculty> Faculties { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<Student> Students { get; set; }
        public DbSet<New> News { get; set; }

        public DbSet<TeacherDepartment> TeacherDepartments { get; set; }

        public DbSet<TeacherSubject> TeacherSubjects { get; set; }

        public DbSet<SubjectDepartment> SubjectDepartments { get; set; }


        public DbSet<Dayofweek> Dayofweeks { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<Timetable> Timetables { get; set; }
        public DbSet<Course> Courses { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<Language> Languages { get; set; }

        public DbSet<Raspisan> Raspisans { get; set; }

        public DbSet<TeacherGroup> TeacherGroups { get; set; }

        public DbSet<Raspisangroup> Raspisangroups { get; set; }

        public DbSet<GroupSubject> GroupSubjects { get; set; }

        public DbSet<FileToGroup> FileToGroups { get; set; }
        public DbSet<FileToTeacher> FileToTeachers { get; set; }

        public DbSet<SubjectList> SubjectLists { get; set; }

        public DbSet<Plase> Plases { get; set; }
        public DbSet<Sezon> Sezons { get; set; }
        public DbSet<Examing> Examings { get; set; }
        public DbSet<RaitingOne> RaitingFirst { get; set; }
        public DbSet<RaitingTwo> RaitingSecond { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<FinalyBal> FinalyBals { get; set; }



        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TeacherSubjectConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherDepartmentConfiguration());
            modelBuilder.ApplyConfiguration(new SubjectDepartmentConfiguration());

            modelBuilder.Entity<ApplicationUser>()
                .Property(u => u.Fio)
                .HasComputedColumnSql("[LastName] + ' ' + [Name] + ' ' + [SurName]");

            base.OnModelCreating(modelBuilder);
           
        }
    }
    public class TeacherSubjectConfiguration : IEntityTypeConfiguration<TeacherSubject>
    {
        public void Configure(EntityTypeBuilder<TeacherSubject> builder)
        {
            builder.HasKey(t => new { t.TeacherId, t.SubjectId });

            builder.HasOne(ts => ts.Teacher)
                .WithMany(t => t.TeacherSubjects)
                .HasForeignKey(ts => ts.TeacherId);

            builder.HasOne(ts => ts.Subject)
                .WithMany(s => s.TeacherSubjects)
                .HasForeignKey(ts => ts.SubjectId);

        }
    }

    public class TeacherDepartmentConfiguration : IEntityTypeConfiguration<TeacherDepartment>
    {
        public void Configure(EntityTypeBuilder<TeacherDepartment> builder)
        {

            builder.HasKey(t => new { t.TeacherId, t.DepartmentId });


            builder.HasOne(td => td.Teacher)
                .WithMany(t => t.TeacherDepartments)
                .HasForeignKey(td => td.TeacherId);

            builder.HasOne(td => td.Department)
                .WithMany(d => d.TeacherDepartments)
                .HasForeignKey(td => td.DepartmentId);
        }
    }

    public class SubjectDepartmentConfiguration : IEntityTypeConfiguration<SubjectDepartment>
    {
        public void Configure(EntityTypeBuilder<SubjectDepartment> builder)
        {

            builder.HasKey(s => new { s.SubjectId, s.DepartmentId });


            builder.HasOne(sd => sd.Subject)
                .WithMany(t => t.SubjectDepartments)
                .HasForeignKey(sd => sd.SubjectId);

            builder.HasOne(sd => sd.Department)
                .WithMany(d => d.SubjectDepartments)
                .HasForeignKey(sd => sd.DepartmentId);
        }
    }
}
