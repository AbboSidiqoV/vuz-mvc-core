﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initia4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_Examings_ExamingId",
                table: "Subjects");

            migrationBuilder.DropForeignKey(
                name: "FK_Teachers_Examings_ExamingId",
                table: "Teachers");

            migrationBuilder.DropIndex(
                name: "IX_Teachers_ExamingId",
                table: "Teachers");

            migrationBuilder.DropIndex(
                name: "IX_Subjects_ExamingId",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "ExamingId",
                table: "Teachers");

            migrationBuilder.DropColumn(
                name: "ExamingId",
                table: "Subjects");

            migrationBuilder.AddColumn<Guid>(
                name: "SubjectId",
                table: "Examings",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TeacherId",
                table: "Examings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Examings_SubjectId",
                table: "Examings",
                column: "SubjectId",
                unique: true,
                filter: "[SubjectId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings",
                column: "TeacherId",
                unique: true,
                filter: "[TeacherId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Examings_Subjects_SubjectId",
                table: "Examings",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Examings_Teachers_TeacherId",
                table: "Examings",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Examings_Subjects_SubjectId",
                table: "Examings");

            migrationBuilder.DropForeignKey(
                name: "FK_Examings_Teachers_TeacherId",
                table: "Examings");

            migrationBuilder.DropIndex(
                name: "IX_Examings_SubjectId",
                table: "Examings");

            migrationBuilder.DropIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Examings");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Examings");

            migrationBuilder.AddColumn<Guid>(
                name: "ExamingId",
                table: "Teachers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ExamingId",
                table: "Subjects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_ExamingId",
                table: "Teachers",
                column: "ExamingId",
                unique: true,
                filter: "[ExamingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_ExamingId",
                table: "Subjects",
                column: "ExamingId",
                unique: true,
                filter: "[ExamingId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_Examings_ExamingId",
                table: "Subjects",
                column: "ExamingId",
                principalTable: "Examings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Teachers_Examings_ExamingId",
                table: "Teachers",
                column: "ExamingId",
                principalTable: "Examings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
