﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initia3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Plases_PlaseId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Students_PlaseId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "PlaseId",
                table: "Students");

            migrationBuilder.AddColumn<Guid>(
                name: "StudentId",
                table: "Plases",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Plases_StudentId",
                table: "Plases",
                column: "StudentId",
                unique: true,
                filter: "[StudentId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Plases_Students_StudentId",
                table: "Plases",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plases_Students_StudentId",
                table: "Plases");

            migrationBuilder.DropIndex(
                name: "IX_Plases_StudentId",
                table: "Plases");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Plases");

            migrationBuilder.AddColumn<Guid>(
                name: "PlaseId",
                table: "Students",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Students_PlaseId",
                table: "Students",
                column: "PlaseId",
                unique: true,
                filter: "[PlaseId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Plases_PlaseId",
                table: "Students",
                column: "PlaseId",
                principalTable: "Plases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
