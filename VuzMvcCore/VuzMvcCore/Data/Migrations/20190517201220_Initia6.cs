﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initia6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings");

            migrationBuilder.CreateIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings",
                column: "TeacherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings");

            migrationBuilder.CreateIndex(
                name: "IX_Examings_TeacherId",
                table: "Examings",
                column: "TeacherId",
                unique: true,
                filter: "[TeacherId] IS NOT NULL");
        }
    }
}
