﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initia2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<byte[]>(
                name: "Avatar",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSignIn",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SurName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fio",
                table: "AspNetUsers",
                nullable: true,
                computedColumnSql: "[LastName] + ' ' + [Name] + ' ' + [SurName]");

            migrationBuilder.CreateTable(
                name: "Faculties",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    DateOfOpen = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    NumberOfKorpus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faculties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Img = table.Column<byte[]>(nullable: true),
                    Tema = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    DateOfOpen = table.Column<DateTime>(nullable: true),
                    FacultyId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Faculties_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "Faculties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Raspisans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FacultyId = table.Column<Guid>(nullable: true),
                    Smestr = table.Column<int>(nullable: false),
                    YearEnd = table.Column<int>(nullable: false),
                    YearStart = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raspisans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Raspisans_Faculties_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "Faculties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<Guid>(nullable: false),
                    Number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Courses_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CourseId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<int>(nullable: true),
                    Symbol = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Groups_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Raspisangroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    RaspisanId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raspisangroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Raspisangroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Raspisangroups_Raspisans_RaspisanId",
                        column: x => x.RaspisanId,
                        principalTable: "Raspisans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sezons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EndDate = table.Column<int>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true),
                    Semestr = table.Column<int>(nullable: false),
                    StartDate = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sezons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sezons_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubjectLists",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    SubjectName = table.Column<string>(nullable: true),
                    TeacherEmail = table.Column<string>(nullable: true),
                    TeacherName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectLists_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Dayofweeks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NameOfWeek = table.Column<string>(nullable: true),
                    RaspisangroupId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dayofweeks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dayofweeks_Raspisangroups_RaspisangroupId",
                        column: x => x.RaspisangroupId,
                        principalTable: "Raspisangroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Examings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SezonId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examings_Sezons_SezonId",
                        column: x => x.SezonId,
                        principalTable: "Sezons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Timetables",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Auditorya = table.Column<string>(nullable: true),
                    DayOfWeekId = table.Column<Guid>(nullable: false),
                    IsHasData = table.Column<bool>(nullable: false),
                    NameOfTeacher = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Time = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timetables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Timetables_Dayofweeks_DayOfWeekId",
                        column: x => x.DayOfWeekId,
                        principalTable: "Dayofweeks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExamingId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Plases_Examings_ExamingId",
                        column: x => x.ExamingId,
                        principalTable: "Examings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExamingId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subjects_Examings_ExamingId",
                        column: x => x.ExamingId,
                        principalTable: "Examings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateOfStartWork = table.Column<DateTime>(nullable: false),
                    ExamingId = table.Column<Guid>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    Staj = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    WorkerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teachers_Examings_ExamingId",
                        column: x => x.ExamingId,
                        principalTable: "Examings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teachers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FinalyBals",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Baxo = table.Column<string>(nullable: true),
                    Finaly = table.Column<int>(nullable: false),
                    PlaseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinalyBals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinalyBals_Plases_PlaseId",
                        column: x => x.PlaseId,
                        principalTable: "Plases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RaitingFirst",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CommonBal = table.Column<int>(nullable: false),
                    Examing = table.Column<int>(nullable: false),
                    Kdz = table.Column<int>(nullable: false),
                    PlaseId = table.Column<Guid>(nullable: false),
                    Posisheniya = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaitingFirst", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RaitingFirst_Plases_PlaseId",
                        column: x => x.PlaseId,
                        principalTable: "Plases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RaitingSecond",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CommonBal = table.Column<int>(nullable: false),
                    Examing = table.Column<int>(nullable: false),
                    Kdz = table.Column<int>(nullable: false),
                    PlaseId = table.Column<Guid>(nullable: false),
                    Posisheniya = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaitingSecond", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RaitingSecond_Plases_PlaseId",
                        column: x => x.PlaseId,
                        principalTable: "Plases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Bal = table.Column<int>(nullable: false),
                    PlaseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sessions_Plases_PlaseId",
                        column: x => x.PlaseId,
                        principalTable: "Plases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EndDate = table.Column<int>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true),
                    GruopId = table.Column<Guid>(nullable: false),
                    PlaseId = table.Column<Guid>(nullable: true),
                    StartDate = table.Column<int>(nullable: false),
                    Stipendy = table.Column<string>(nullable: true),
                    Study = table.Column<string>(nullable: true),
                    TypeStudy = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Plases_PlaseId",
                        column: x => x.PlaseId,
                        principalTable: "Plases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GroupSubjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true),
                    SubjectId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupSubjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupSubjects_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupSubjects_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubjectDepartments",
                columns: table => new
                {
                    SubjectId = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectDepartments", x => new { x.SubjectId, x.DepartmentId });
                    table.ForeignKey(
                        name: "FK_SubjectDepartments_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectDepartments_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FileToGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatePost = table.Column<string>(nullable: true),
                    GroupId = table.Column<Guid>(nullable: true),
                    NameFile = table.Column<string>(nullable: true),
                    PathFile = table.Column<string>(nullable: true),
                    SubjectName = table.Column<string>(nullable: true),
                    TeacherId = table.Column<Guid>(nullable: true),
                    TypeFile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileToGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileToGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FileToGroups_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TeacherDepartments",
                columns: table => new
                {
                    TeacherId = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherDepartments", x => new { x.TeacherId, x.DepartmentId });
                    table.ForeignKey(
                        name: "FK_TeacherDepartments_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherDepartments_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    TeacherId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeacherGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherGroups_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherSubjects",
                columns: table => new
                {
                    TeacherId = table.Column<Guid>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherSubjects", x => new { x.TeacherId, x.SubjectId });
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FileToTeachers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DatePost = table.Column<string>(nullable: true),
                    GroupId = table.Column<Guid>(nullable: false),
                    NameFile = table.Column<string>(nullable: true),
                    PathFile = table.Column<string>(nullable: true),
                    StudentId = table.Column<Guid>(nullable: false),
                    SubjectName = table.Column<string>(nullable: true),
                    TypeFile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileToTeachers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileToTeachers_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FileToTeachers_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Courses_DepartmentId",
                table: "Courses",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Dayofweeks_RaspisangroupId",
                table: "Dayofweeks",
                column: "RaspisangroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_FacultyId",
                table: "Departments",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_Examings_SezonId",
                table: "Examings",
                column: "SezonId");

            migrationBuilder.CreateIndex(
                name: "IX_FileToGroups_GroupId",
                table: "FileToGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_FileToGroups_TeacherId",
                table: "FileToGroups",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_FileToTeachers_GroupId",
                table: "FileToTeachers",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_FileToTeachers_StudentId",
                table: "FileToTeachers",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_FinalyBals_PlaseId",
                table: "FinalyBals",
                column: "PlaseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Groups_CourseId",
                table: "Groups",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_LanguageId",
                table: "Groups",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupSubjects_GroupId",
                table: "GroupSubjects",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupSubjects_SubjectId",
                table: "GroupSubjects",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Plases_ExamingId",
                table: "Plases",
                column: "ExamingId");

            migrationBuilder.CreateIndex(
                name: "IX_RaitingFirst_PlaseId",
                table: "RaitingFirst",
                column: "PlaseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RaitingSecond_PlaseId",
                table: "RaitingSecond",
                column: "PlaseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Raspisangroups_GroupId",
                table: "Raspisangroups",
                column: "GroupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Raspisangroups_RaspisanId",
                table: "Raspisangroups",
                column: "RaspisanId");

            migrationBuilder.CreateIndex(
                name: "IX_Raspisans_FacultyId",
                table: "Raspisans",
                column: "FacultyId",
                unique: true,
                filter: "[FacultyId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_PlaseId",
                table: "Sessions",
                column: "PlaseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sezons_GroupId",
                table: "Sezons",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_GroupId",
                table: "Students",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_PlaseId",
                table: "Students",
                column: "PlaseId",
                unique: true,
                filter: "[PlaseId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Students_UserId",
                table: "Students",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectDepartments_DepartmentId",
                table: "SubjectDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectLists_GroupId",
                table: "SubjectLists",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_ExamingId",
                table: "Subjects",
                column: "ExamingId",
                unique: true,
                filter: "[ExamingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherDepartments_DepartmentId",
                table: "TeacherDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherGroups_GroupId",
                table: "TeacherGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherGroups_TeacherId",
                table: "TeacherGroups",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_ExamingId",
                table: "Teachers",
                column: "ExamingId",
                unique: true,
                filter: "[ExamingId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_UserId",
                table: "Teachers",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjects_SubjectId",
                table: "TeacherSubjects",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Timetables_DayOfWeekId",
                table: "Timetables",
                column: "DayOfWeekId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FileToGroups");

            migrationBuilder.DropTable(
                name: "FileToTeachers");

            migrationBuilder.DropTable(
                name: "FinalyBals");

            migrationBuilder.DropTable(
                name: "GroupSubjects");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "RaitingFirst");

            migrationBuilder.DropTable(
                name: "RaitingSecond");

            migrationBuilder.DropTable(
                name: "Sessions");

            migrationBuilder.DropTable(
                name: "SubjectDepartments");

            migrationBuilder.DropTable(
                name: "SubjectLists");

            migrationBuilder.DropTable(
                name: "TeacherDepartments");

            migrationBuilder.DropTable(
                name: "TeacherGroups");

            migrationBuilder.DropTable(
                name: "TeacherSubjects");

            migrationBuilder.DropTable(
                name: "Timetables");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "Dayofweeks");

            migrationBuilder.DropTable(
                name: "Plases");

            migrationBuilder.DropTable(
                name: "Raspisangroups");

            migrationBuilder.DropTable(
                name: "Examings");

            migrationBuilder.DropTable(
                name: "Raspisans");

            migrationBuilder.DropTable(
                name: "Sezons");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Faculties");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Fio",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IsSignIn",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SurName",
                table: "AspNetUsers");
        }
    }
}
