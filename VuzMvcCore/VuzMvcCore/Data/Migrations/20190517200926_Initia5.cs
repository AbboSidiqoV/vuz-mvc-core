﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initia5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Plases_StudentId",
                table: "Plases");

            migrationBuilder.CreateIndex(
                name: "IX_Plases_StudentId",
                table: "Plases",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Plases_StudentId",
                table: "Plases");

            migrationBuilder.CreateIndex(
                name: "IX_Plases_StudentId",
                table: "Plases",
                column: "StudentId",
                unique: true,
                filter: "[StudentId] IS NOT NULL");
        }
    }
}
