﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VuzMvcCore.Data.Migrations
{
    public partial class Initial10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Raspisans_FacultyId",
                table: "Raspisans");

            migrationBuilder.CreateIndex(
                name: "IX_Raspisans_FacultyId",
                table: "Raspisans",
                column: "FacultyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Raspisans_FacultyId",
                table: "Raspisans");

            migrationBuilder.CreateIndex(
                name: "IX_Raspisans_FacultyId",
                table: "Raspisans",
                column: "FacultyId",
                unique: true,
                filter: "[FacultyId] IS NOT NULL");
        }
    }
}
