﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Data;
using VuzMvcCore.Models;
using VuzMvcCore.Models.ViewModels;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Controllers
{
    public class StudentController : Controller
    {
        ApplicationDbContext _context;
        IHostingEnvironment _enviroment;
        public StudentController(ApplicationDbContext db, UserManager<ApplicationUser> user, IHostingEnvironment env) 
        {
            _context = db;
            _enviroment = env;
        }

        public async Task<IActionResult> Main()
        {
            var user = await _context.Users.Include(u => u.Student).ThenInclude(s => s.Group)
                .ThenInclude(g => g.Course).ThenInclude(c => c.Department).ThenInclude(d => d.Faculty).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            return View(user);

        } 




        public async Task<IActionResult> ChangeData()
        {
            ApplicationUser user = await _context.Users.Include(u => u.Student)
                .ThenInclude(s => s.Group).ThenInclude(g => g.Course).ThenInclude(c => c.Department).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeData(IFormFile avatar, ApplicationUser u)
        {
            ApplicationUser user = await _context.Users.Include(us => us.Student)
                .ThenInclude(r => r.Group).ThenInclude(g => g.Course).FirstOrDefaultAsync(us => us.Id == u.Id);

            if (user != null)
            {
                if (user.Student.StartDate == 0)
                {
                    user.Student.StartDate = DateTime.Now.Year - user.Student.Group.Course.Number;
                    user.Student.EndDate = user.Student.StartDate + 4;
                }


                if (avatar != null)
                {
                    byte[] imageData = null;
                    // считываем переданный файл в массив байтов
                    using (var binaryReader = new BinaryReader(avatar.OpenReadStream()))
                    {
                        imageData = binaryReader.ReadBytes((int)avatar.Length);
                    }
                    // установка массива байтов
                    user.Avatar = imageData;

                }
                user.Address = u.Address;
                user.DateOfBirth = u.DateOfBirth;
                user.PhoneNumber = u.PhoneNumber;
                user.LastName = u.LastName;
                user.SurName = u.SurName;
                user.Name = u.Name;
                if (u.DateOfBirth != null)
                    user.Age = DateTime.Now.Year - u.DateOfBirth.Value.Year;
                else
                    user.Age = 0;

                await _context.SaveChangesAsync();
                return RedirectToAction("Main");
            }
            return NotFound();


        }


        public async Task<IActionResult> Timestamp()
        {
            var user = await _context.Users.Include(u => u.Student)
                .ThenInclude(s => s.Group).ThenInclude(g => g.Raspisangroup).ThenInclude(r => r.Dayofweeks).ThenInclude(d => d.Timetables)
                .FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            Raspisangroup raspisangroup = user.Student.Group.Raspisangroup;

            return View(raspisangroup);
        }


        public async Task<IActionResult> SubjectTags(Guid? id)
        {
            SubjectList subject = await _context.SubjectLists.Include(s => s.Group).ThenInclude(g => g.FileToGroups)
                .Include(s => s.Group).ThenInclude(g => g.Course)
                .Include(s => s.Group).ThenInclude(g => g.Language).FirstOrDefaultAsync(sub => sub.Id == id);
            
            ApplicationUser user = await _context.Users.Include(u => u.Student).ThenInclude(s => s.FileToTeachers).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (subject != null && user != null)
            {
                StudentTagsViewModel view = new StudentTagsViewModel();
                view.SubjectList = subject;
                view.CurrUser = user;
                var filesT = subject.Group.FileToGroups;
                foreach (var item in filesT)
                {
                    if (item.SubjectName == subject.SubjectName)
                    {
                        view.FileToGroups.Add(item);
                    }
                }

                var fileS = user.Student.FileToTeachers;

                foreach (var item in fileS)
                {
                    if (item.SubjectName == subject.SubjectName)
                    {
                        view.FileToTeachers.Add(item);
                    }
                }

                return View(view);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> SubjectTags(IFormFile uploadedFile,  string groupN, string groupS, string courseN, string groupL, Guid? id)
        {
            ApplicationUser user = await _context.Users.Include(u => u.Student).ThenInclude(s => s.FileToTeachers)
                .Include(u => u.Student).ThenInclude(s => s.Group).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            SubjectList subjectList = await _context.SubjectLists.FirstOrDefaultAsync(s => s.Id == id);


            if (subjectList != null)
            {
                FileToTeacher file = new FileToTeacher();

                file.Student = user.Student;
                file.Group = user.Student.Group;


                if (uploadedFile != null)
                {
                    string pathStudent = $"{_enviroment.WebRootPath}/FileTeacher/{subjectList.TeacherEmail}";

                    if (!Directory.Exists(pathStudent))
                    {
                        Directory.CreateDirectory(pathStudent);
                    }

                    string subPath = $"{pathStudent}/{courseN}_{groupN}{groupS}{groupL.Substring(0, 1)}";

                    if (!Directory.Exists(subPath))
                    {
                        Directory.CreateDirectory(subPath);
                    }
                    // путь к папке Files
                    string path = $"{subPath}/{uploadedFile.FileName}";

                    if (!Directory.Exists(path))
                    {
                        // сохраняем файл в папку Files в каталоге wwwroot
                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        file.PathFile = path;
                        file.NameFile = uploadedFile.FileName;
                        file.SubjectName = subjectList.SubjectName;
                        file.TypeFile = uploadedFile.ContentType;
                        file.DatePost = DateTime.Now.ToString();

                        user.Student.FileToTeachers.Add(file);
                        await _context.SaveChangesAsync();
                    }

                    return RedirectToAction("SubjectTags", "Student", new { id = subjectList.Id});

                }

                return BadRequest();
            }
            return NotFound();
           

        }

        public async Task<IActionResult> Examing(Guid? id)
        {

            ApplicationUser user =  await _context.Users
                .Include(u => u.Student).ThenInclude(s => s.Group).ThenInclude(g => g.Sezons)
                .FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            Sezon sezon = await _context.Sezons
                .Include(s => s.Examings).ThenInclude(e => e.Subject)
                .Include(s => s.Examings).ThenInclude(e => e.Teacher).ThenInclude(t => t.User)
                .Include(s => s.Examings).ThenInclude(e => e.Plases)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.RaitingOne)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.Student).ThenInclude(s => s.User)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.RaitingTwo)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.Sessions)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.FinalyBals)
                .FirstOrDefaultAsync(s => s.Id == user.Student.Group.Sezons.Last().Id);

            SubjectList list = await _context.SubjectLists.FirstOrDefaultAsync(s => s.Id == id);
            if (list != null)
            {
                StudentExamingViewModel view = new StudentExamingViewModel();



                view.Examing = sezon.Examings.FirstOrDefault(e => e.Subject.Name == list.SubjectName);
                view.Sezon = sezon;
                view.User = user;

                ViewData["id"] = id;
                return View(view);
            }
            return NotFound();
        }




        [HttpPost]
        public IActionResult GetFile(string path, string file, string type)
        {
            return PhysicalFile(path, type, file);
        }
    }
}