﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Data;
using VuzMvcCore.Models;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.ViewModels;

namespace VuzMvcCore.Controllers
{
    [Route("Admin/[controller]/[action]")]
    //[Authorize(Roles = "admin")]
    public class RoleController : Controller
    {
        UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole> _roleManager;
        ApplicationDbContext _context;

        public RoleController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            ApplicationDbContext db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = db;
        }

        public IActionResult Main()
        {
            return View("~/Views/Admin/Role/Main.cshtml", _roleManager.Roles.ToList());
        }

        public IActionResult Create() => View("~/Views/Admin/Role/Create.cshtml");

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Main", "Role");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Main");
        }

        public async Task<IActionResult> Edit(string userId)
        {
            // получаем пользователя
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();

                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View("~/Views/Admin/Role/Edit.cshtml", model);
            }

            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {

            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(u => u.Id == userId);
            if (user != null)
            {
                if (user.Teacher == null)
                {
                    if (roles[0] == "teacher")
                    {
                        Teacher teacher = new Teacher();
                        teacher.DateOfStartWork = DateTime.Now;
                        user.Teacher = teacher;

                    }
                    else
                    {
                        user.Teacher = null;
                    }

                }

                if (user.Student == null)
                {

                    if (roles[0] == "student")
                    {
                        Student student = new Student();
                        user.Student = student;

                    }
                    else
                    {
                        user.Student = null;
                    }

                }


                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                // получаем все роли
                var allRoles = _roleManager.Roles.ToList();
                // получаем список ролей, которые были добавлены
                var addedRoles = roles.Except(userRoles);
                // получаем роли, которые были удалены
                var removedRoles = userRoles.Except(roles);


                IdentityResult res = await _userManager.UpdateAsync(user);



                if (user.Teacher != null || user.Student != null || roles[0] == "admin")
                {
                    user.IsSignIn = true;
                }
                else
                {
                    user.IsSignIn = false;
                    await _userManager.RemoveFromRolesAsync(user, removedRoles);
                    return RedirectToAction("Main", "User");

                }

                await _userManager.AddToRolesAsync(user, addedRoles);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                await _context.SaveChangesAsync();

                return RedirectToAction("Main", "User");
            }

            return NotFound();


            // получаем пользователя
           
        }
    }
}