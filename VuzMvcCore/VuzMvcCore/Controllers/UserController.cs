﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Models;
using VuzMvcCore.Data;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.ViewModels;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Controllers
{
    [Route("Admin/[controller]/[action]")]
    //[Authorize(Roles = "admin")]
    public class UserController : Controller
    {

        UserManager<ApplicationUser> _userManager;

        AdminUserViewModel views;
        ApplicationDbContext _context;


        public UserController(UserManager<ApplicationUser> userManager,
            ApplicationDbContext db)
        {

            _userManager = userManager;
            _context = db;

            views = new AdminUserViewModel();
        }


        public async Task<IActionResult> Main()
        {
            Filter filter = new Filter
            {
                Active = "",
                Role = ""
            };

            views.ApplicationUsers = await _userManager.Users.Where(u => u.Email != "admin@mail.ru" && u.EmailConfirmed).ToListAsync();

            views.Filter = filter;
            return View("~/Views/Admin/User/Main.cshtml", views);
        }

        public async Task<IActionResult> Detail(string id)
        {

            ApplicationUser user = await _context.Users
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherDepartments).ThenInclude(t => t.Department)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(t => t.Group)
                .Include(u => u.Student).ThenInclude(t => t.Group).ThenInclude(t => t.Course).ThenInclude(g => g.Department).ThenInclude(d => d.Faculty)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user != null)
            {
                return View(user);
            }
            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {

            ApplicationUser user = await _context.Users.Include(u => u.Teacher).Include(u => u.Student).FirstOrDefaultAsync(u => u.Id == id);
            if (user != null)
            {
                 _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Main");
        }


        [HttpGet]
        public async Task<IActionResult> SearchUsers(string email)
        {
            Filter filter = new Filter
            {
                Active = "",
                Role = ""
            };

            IEnumerable<ApplicationUser> users;
            if (string.IsNullOrEmpty(email))
            {
                users = await _userManager.Users.Where(u => u.Email != "admin@mail.ru" && u.EmailConfirmed).ToListAsync();
            }
            else
            {
                users = await _userManager.Users.Where(u => u.Email.Contains(email) && u.Email != "admin@mail.ru" && u.EmailConfirmed).ToListAsync();
            }
            views.ApplicationUsers = users;
            views.Filter = filter;

            return View("~/Views/Admin/User/Main.cshtml", views);
        }


        public async Task<IActionResult> EditData(string userid)
        {
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userid);

            if (user != null)
            {
                Task<IList<string>> roles = _userManager.GetRolesAsync(user);

                if (roles != null)
                {
                    foreach (var item in roles.Result)
                    {
                        if (item == "teacher")
                        {
                            return RedirectToAction("EditTeacher", new { userid });
                        }
                        else if (item == "student")
                        {

                            return RedirectToAction("EditStudent",  new { userid });
                        }
                        else
                        {
                            return BadRequest("Этому пользователю не доступно данный пункт");
                        }
                    }
                }
            }
            return NotFound();
        }

        public async Task<IActionResult> EditTeacher(string  userid)
        {
            ApplicationUser user = await _context.Users.Include(u => u.Teacher)
                .ThenInclude(u => u.TeacherDepartments).ThenInclude(d => d.Department).FirstOrDefaultAsync(u => u.Id == userid);
            

            if (user != null)
            {
                TeacherDepartViewModel view = new TeacherDepartViewModel();

                ViewData["id"] = user.Id;
                ViewData["email"] = user.Email;

                view.TeacherDeparts = user.Teacher.TeacherDepartments.Select(ds => ds.Department).Select(s => s.Name).ToList();

                view.Faculties = await _context.Faculties.Include(c => c.Departments).ToListAsync();

                return View(view);




            }



            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> EditTeacher(List<string> checks, string id)
        {
            Teacher teacher = await _context.Teachers.Include(u => u.User)
                .Include(u => u.TeacherDepartments).ThenInclude(d => d.Department).FirstOrDefaultAsync(u => u.User.Id == id);


            if (teacher != null)
            {
                List<string> deps = teacher.TeacherDepartments.Select(ds => ds.Department).Select(d => d.Name).ToList();


                IEnumerable<string> newsubs = checks.Except(deps);

                IEnumerable<string> removedsubs = deps.Except(checks);



                List<Department> adDeps = new List<Department>();

                List<Department> reDeps = new List<Department>();

                foreach (var item in newsubs)
                {
                    adDeps.Add(_context.Departments.FirstOrDefault(c => c.Name == item));

                }

                foreach (var item in removedsubs)
                {
                    reDeps.Add(_context.Departments.FirstOrDefault(c => c.Name == item));

                }


                if (adDeps.Count > 0)
                {
                    foreach (var item in adDeps)
                    {
                        teacher.TeacherDepartments.Add(new TeacherDepartment { DepartmentId = item.Id, TeacherId = teacher.Id });

                    }

                    _context.SaveChanges();
                }


                if (reDeps.Count > 0)
                {
                    foreach (var item in reDeps)
                    {
                        TeacherDepartment subjectDepartment = item.TeacherDepartments.FirstOrDefault(s =>
                       s.TeacherId == teacher.Id && s.DepartmentId == item.Id);
                        teacher.TeacherDepartments.Remove(subjectDepartment);

                    }

                    _context.SaveChanges();
                }




                return RedirectToAction("EditTeacher", new { userid = teacher.User.Id });

            }
            return NotFound();

        }


        


        public async Task<IActionResult> EditStudent(string userid)
        {
            ApplicationUser user = await _context.Users.Include(u => u.Student)
                .ThenInclude(u => u.Group).ThenInclude(d => d.Course)
                .ThenInclude(c => c.Department).ThenInclude(f => f.Faculty).FirstOrDefaultAsync(u => u.Id == userid);


            if (user != null)
            {
                StudentViewModel view = new StudentViewModel
                {
                    Faculties = await _context.Faculties.ToListAsync(),
                    Departments = await _context.Departments.Include(d => d.Faculty).ToListAsync(),
                    Courses = new List<string> { "1", "2", "3", "4" },
                    Groups = await _context.Groups.Include(g => g.Course).Include(g => g.Language).ToListAsync(),
                    FacName = user.Student.Group?.Course?.Department?.Faculty?.Name.ToString(),
                    CourseName= user.Student.Group?.Course?.Number.ToString(),
                    DepName= user.Student.Group?.Course?.Department.Name.ToString(),
                    GroupName = user.Student.Group?.Title.ToString()

                };


                ViewData["id"] = user.Id;
                ViewData["email"] = user.Email;


                
                



                return View(view);




            }



            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> EditStudent(string course, string depId, string facId, string groupId, string userid)
        {
            if (string.IsNullOrEmpty(facId) || string.IsNullOrEmpty(course) ||
                string.IsNullOrEmpty(depId) || string.IsNullOrEmpty(groupId))
            {
                return BadRequest();
            }

            ApplicationUser user = await _context.Users.Include(s => s.Student).FirstOrDefaultAsync(u => u.Id.ToString() == userid);

            



            if (user != null)
            {
                Student student = user.Student;
                Faculty faculty = await _context.Faculties.Include(c => c.Departments).ThenInclude(d => d.Courses)
                                            .ThenInclude(g => g.Groups).ThenInclude(g => g.Sezons).FirstOrDefaultAsync(f => f.Id.ToString() == facId);


                if (faculty != null)
                {
                    Department dep = faculty.Departments.FirstOrDefault(cor => cor.Id.ToString() == depId);

                    if (dep != null)
                    {
                        Course c = dep.Courses.FirstOrDefault(de => de.Number == int.Parse(course));

                        if (c != null)
                        {
                            Group g = c.Groups.FirstOrDefault(go => go.Id.ToString() == groupId);

                            if (g != null)
                            {
                              
                                g.Students.Add(student);
                                
                                await _context.SaveChangesAsync();
                                return RedirectToAction("EditStudent", new { userid });
                            }
                        }
                    }

                }

            }
            

            return NotFound();


        }


        [HttpPost]
        public async Task<IActionResult> Filter(Filter filter)
        {
            IEnumerable<ApplicationUser> users = await _userManager.Users.Where(u => u.Email != "admin@mail.ru" && u.EmailConfirmed).ToListAsync();
            List<ApplicationUser> appusers = new List<ApplicationUser>();

            bool? flag;

            if (filter.Active == "1")
                flag = true;
            else if (filter.Active == "0")
                flag = false;
            else
                flag = null;


            if (!string.IsNullOrEmpty(filter.Role))
            {


                foreach (var us in users)
                {
                    if(await _userManager.IsInRoleAsync(us, filter.Role))
                    {
                        appusers.Add(us);
                    }

                }

                if (flag != null)
                {
                    appusers = appusers.Where(u => u.IsSignIn == flag  ).ToList();
                }
                
            }
            else if(flag != null)
            {
                appusers = users.Where(u => u.IsSignIn == flag).ToList();

            }
            else
            {
                appusers = users.ToList();
            }

            views.ApplicationUsers = appusers;
            views.Filter = filter;


            return View("~/Views/Admin/User/Main.cshtml", views);
        }


    }
}