﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Data;
using VuzMvcCore.Models.VUZ;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using VuzMvcCore.Models.AccountViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using VuzMvcCore.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using VuzMvcCore.Models.ViewModels;
using VuzMvcCore.Models.Relations;

namespace VuzMvcCore.Controllers
{
    //[Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _appEnviroment;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public AdminController(ApplicationDbContext context, IHostingEnvironment appEnviroment,
                                SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _appEnviroment = appEnviroment;
            _signInManager = signInManager;

        }




        [Route("Admin/Main")]
        public IActionResult Main()
        {

            return View();
        }





        public IActionResult Faculties()
        {
            IEnumerable<Faculty> fac = _context.Faculties
                    .Include(f => f.Departments).ThenInclude(d => d.Courses).ThenInclude(g => g.Groups)
                    .OrderBy(f => f.NumberOfKorpus).ToList();

            return View(fac);

        }

        [HttpGet]
        public IActionResult CreateFaculty()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFaculty(Faculty fac)
        {
            if (!ModelState.IsValid)
            {
                return View(fac);

            }
            else
            {

                _context.Faculties.Add(fac);
                _context.SaveChanges();
                await CreateRaspisanForFaculty(fac);
                return RedirectToAction("Faculties");

            }



        }

        private async Task InitialCource(Department dep)
        {
            Course c1 = new Course() { Number = 1, Department = dep };
            Course c2 = new Course() { Number = 2, Department = dep };
            Course c3 = new Course() { Number = 3, Department = dep };
            Course c4 = new Course() { Number = 4, Department = dep };

            _context.Courses.Add(c1);
            _context.Courses.Add(c2);
            _context.Courses.Add(c3);
            _context.Courses.Add(c4);

            await _context.SaveChangesAsync();
        }

        [HttpGet]
        public IActionResult EditFaculty(string id)
        {
            Faculty fac = _context.Faculties.FirstOrDefault(f => f.Id.ToString() == id);

            if (fac == null)
            {
                return NotFound();
            }

            return View(fac);
        }

        [HttpPost]
        public IActionResult EditFaculty(Faculty faculty)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (faculty != null)
            {
                _context.Faculties.Update(faculty);
                _context.SaveChanges();
                return RedirectToAction("Faculties", "Admin");
            }
            return View(faculty);

        }

        [HttpGet]
        public IActionResult DeleteFaculty(string id)
        {
            Faculty faculty = _context.Faculties.SingleOrDefault(f => f.Id.ToString() == id);

            if (faculty != null)
            {
                return Json(faculty);
            }
            return NotFound();


        }

        [HttpPost]
        public IActionResult DeleteFaculty(Faculty faculty)
        {
            if (faculty != null)
            {
                _context.Faculties.Remove(faculty);
                _context.SaveChanges();
                return Ok();

            }
            else
            {
                return NotFound();
            }

        }


        [HttpGet]
        public IActionResult CreateDepartment(string id)
        {
            ViewData["facultyId"] = id;
            return View();

        }


        [HttpPost]
        public async Task<IActionResult> CreateDepartment(Department department, string facultyId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();

            }



            Faculty fac = _context.Faculties.SingleOrDefault(с => с.Id.ToString() == facultyId);

            if (fac != null)
            {
                department.Id = Guid.NewGuid();

                _context.Departments.Add(department);
                fac.Departments.Add(department);
                _context.SaveChanges();
                await InitialCource(department);
                return RedirectToAction("Faculties");
            }
            else
            {
                return NotFound();
            }



        }
        [HttpGet]
        public IActionResult DeleteDepartment(string id)
        {

            Department department = _context.Departments.SingleOrDefault(f => f.Id.ToString() == id);

            if (department != null)
            {
                return Json(department);
            }
            return NotFound();



        }

        [HttpPost]
        public IActionResult DeleteDepartment(Department department)
        {

            if (department != null)
            {
                _context.Departments.Remove(department);
                _context.SaveChanges();
                return Ok();
            }
            return NotFound();


        }

        [HttpGet]
        public async Task<IActionResult> EditDepartment(string id)
        {

            if (!string.IsNullOrEmpty(id))
            {
                Department department = await _context.Departments.SingleOrDefaultAsync(d => d.Id.ToString() == id);
                if (department != null)
                {
                    return View(department);
                }
                else
                {
                    return NotFound(department);
                }

            }
            else
            {
                return BadRequest();
            }





        }

        [HttpPost]
        public IActionResult EditDepartment(Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (department != null)
            {
                _context.Departments.Update(department);
                _context.SaveChanges();
                return RedirectToAction("Faculties");
            }
            return NotFound();
        }


        [HttpGet]
        public IActionResult CreateGroup(string Id)
        {
            ViewData["courseId"] = Id;

            var languages = _context.Languages.ToList();

            List<string> bukvs = new List<string>
            {
                "",
                "A",
                "B",
                "C"
            };

            ViewBag.Languages = languages;
            ViewBag.Bukvs = bukvs;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateGroup(Group group, string courseId)
        {
            var course = await _context.Courses.FirstOrDefaultAsync(c => c.Id.ToString() == courseId);

            var lang = _context.Languages.SingleOrDefault(l => l.Id == group.LanguageId);

            if (lang != null && course != null)
            {
                if (group.Symbol == null)
                {
                    group.Symbol = "";
                }
                group.Language = lang;

                group.Id = Guid.NewGuid();
                _context.Groups.Add(group);
                course.Groups.Add(group);

                _context.SaveChanges();
                await CreateRaspisanForGroup(group);
                await CreateExamForGroup(group);
                return RedirectToAction("Faculties");
            }


            return View(group);


        }

        private async Task CreateExamForGroup(Group group)
        {
            Sezon sezon = new Sezon();

            if (1 < DateTime.Now.Month && DateTime.Now.Month < 7)
            {
                sezon.Semestr = 2;
                sezon.StartDate = DateTime.Now.Year - 1;
                sezon.EndDate = DateTime.Now.Year;
            }
            if (8 < DateTime.Now.Month && DateTime.Now.Month < 13)
            {
                sezon.Semestr = 1;
                sezon.StartDate = DateTime.Now.Year;
                sezon.EndDate = DateTime.Now.Year + 1;
            }

            group.Sezons.Add(sezon);

            await _context.SaveChangesAsync();



        }

        [HttpGet]
        public async Task<IActionResult> EditGroup(string id)
        {

            var group = await _context.Groups.SingleOrDefaultAsync(g => g.Id.ToString() == id);

            if (group != null)
            {
                var languages = _context.Languages.ToList();
                List<string> bukvs = new List<string>
                {
                    "",
                    "A",
                    "B",
                    "C"
                };

                ViewBag.Languages = languages;
                ViewBag.Bukvs = bukvs;


                return View(group);

            }

            return NotFound(group);

        }

        [HttpPost]
        public IActionResult EditGroup(Group g)
        {
            if (g != null)
            {
                var lang = _context.Languages.SingleOrDefault(l => l.Id == g.LanguageId);
                g.Language = lang;
                _context.Groups.Update(g);
                _context.SaveChanges();
                return RedirectToAction("Faculties");


            }
            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> DeleteGroup(string id)
        {

            Group gr = await _context.Groups.SingleOrDefaultAsync(f => f.Id.ToString() == id);

            if (gr != null)
            {
                return Json(gr);
            }
            return NotFound();



        }

        [HttpPost]
        public IActionResult DeleteGroup(Group group)
        {

            if (group != null)
            {
                _context.Groups.Remove(group);
                _context.SaveChanges();
                return Ok();
            }
            return NotFound();


        }

        [HttpGet]
        public IActionResult News()
        {
            var news = _context.News.OrderByDescending(n => n.Id).ToList();
            return View(news);
        }

        public IActionResult CreateNew()
        {


            return View();
        }


        [HttpPost]
        public IActionResult CreateNew(New n)
        {
            if (ModelState.IsValid)
            {
                n.Date = DateTime.Now;
                _context.News.Add(n);
                _context.SaveChanges();
                return RedirectToAction("News");
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AddSubjectToDepart(string id)
        {
            ChangeSubjectViewNodel view = new ChangeSubjectViewNodel();

            Department depart = await _context.Departments.Include(d => d.SubjectDepartments).ThenInclude(s => s.Subject)
                .SingleOrDefaultAsync(d => d.Id.ToString() == id);

            if (depart != null)
            {

                ViewData["dep"] = id;
                ViewData["depName"] = depart.Name;
                var subs = depart.SubjectDepartments.Select(ds => ds.Subject).ToList();

                view.Subjects = _context.Subjects.ToList();
                view.DepartSubjects = subs.Select(s => s.Name).ToList();

                return View(view);
            }
            return NotFound();

        }


        [HttpPost]
        public async Task<IActionResult> AddSubjectToDepart(List<string> checks, string dep)
        {
            Department depart = await _context.Departments.Include(d => d.SubjectDepartments)
                    .ThenInclude(s => s.Subject).SingleOrDefaultAsync(d => d.Id.ToString() == dep);



            if (depart != null)
            {

                List<string> deps = depart.SubjectDepartments.Select(ds => ds.Subject).Select(d => d.Name).ToList();


                IEnumerable<string> newsubs = checks.Except(deps);

                IEnumerable<string> removedsubs = deps.Except(checks);



                List<Subject> adSubs = new List<Subject>();

                List<Subject> reSubs = new List<Subject>();

                foreach (var item in newsubs)
                {
                    adSubs.Add(_context.Subjects.FirstOrDefault(c => c.Name == item));

                }

                foreach (var item in removedsubs)
                {
                    reSubs.Add(_context.Subjects.FirstOrDefault(c => c.Name == item));

                }


                if (adSubs.Count > 0)
                {
                    foreach (var item in adSubs)
                    {
                        depart.SubjectDepartments.Add(new SubjectDepartment { SubjectId = item.Id, DepartmentId = depart.Id });

                    }

                    _context.SaveChanges();
                }


                if (reSubs.Count > 0)
                {
                    foreach (var item in reSubs)
                    {
                        SubjectDepartment subjectDepartment = item.SubjectDepartments.FirstOrDefault(s =>
                       s.SubjectId == item.Id && s.DepartmentId == depart.Id);
                        depart.SubjectDepartments.Remove(subjectDepartment);

                    }

                    _context.SaveChanges();
                }




                return RedirectToAction("AddSubjectToDepart", "Admin", new { depart.Id });

            }


            return NotFound();

        }

        [HttpGet]
        public IActionResult Subjects()
        {

            var subs = _context.Subjects.Distinct().ToList();

            return View(subs);

        }

        [HttpPost]
        public async Task<IActionResult> Subjects(List<string> subject)
        {
            List<Subject> subs = new List<Subject>();
            foreach (var item in subject)
            {
                if (string.IsNullOrEmpty(item)) continue;
                subs.Add(new Subject { Name = item.ToString() });
            }

            if (subs.Count > 0)
            {
                await _context.Subjects.AddRangeAsync(subs);
                await _context.SaveChangesAsync();


                return RedirectToAction("Subjects", "Admin");
            }

            return BadRequest();


        }

        //[HttpPost]
        //public IActionResult DeleteSubject(string id)
        //{
        //    var sub = _context.Subjects.FirstOrDefault(s => s.Id.ToString() == id);

        //    if (sub != null)
        //    {
        //        _context.Subjects.Remove(sub);
        //        _context.SaveChanges();
        //        return RedirectToAction("Subjects");
        //    }
        //    return NotFound();
        //}



        public async Task CreateRaspisanForFaculty(Faculty faculty)
        {

            Raspisan ras = new Raspisan();

            if (1 < DateTime.Now.Month && DateTime.Now.Month < 7)
            {
                ras.Smestr = 2;
                ras.YearStart = DateTime.Now.Year - 1;
                ras.YearEnd = DateTime.Now.Year;
            }
            if (8 < DateTime.Now.Month && DateTime.Now.Month < 13)
            {
                ras.Smestr = 1;
                ras.YearStart = DateTime.Now.Year;
                ras.YearEnd = DateTime.Now.Year + 1;
            }
            _context.Raspisans.Add(ras);
            faculty.Raspisans.Add(ras);
            await _context.SaveChangesAsync();
        }
        public async Task CreateRaspisanForGroup(Group gr)
        {
            Group group = await _context.Groups.Include(g => g.Course).ThenInclude(c => c.Department).ThenInclude(d => d.Faculty).ThenInclude(f => f.Raspisans)
                            .FirstOrDefaultAsync(g => g.Id == gr.Id);

            if (group != null)
            {
                Raspisan raspisan = group.Course?.Department?.Faculty?.Raspisans.Last();
                if (raspisan != null)
                {
                    Dayofweek dw;
                    Timetable tm;
                    Raspisangroup child;
                    List<string> days = new List<string>
                    {
                        "Душанбе",
                        "Сешанбе",
                        "Чоршанбе",
                        "Панчшанбе",
                        "Чумъа",
                        "Шанбе"

                    };



                    child = new Raspisangroup();
                    child.Group = group;

                    for (int i = 0; i < days.Count; i++)
                    {
                        dw = new Dayofweek();
                        dw.NameOfWeek = days[i];

                        for (int j = 8; j < 19; j++)
                        {
                            tm = new Timetable();
                            tm.Time = $"{j} - {j}:50";
                            dw.Timetables.Add(tm);
                            _context.Timetables.Add(tm);
                        }
                        _context.Dayofweeks.Add(dw);
                        child.Dayofweeks.Add(dw);

                    }

                    raspisan.Raspisangroups.Add(child);

                    await _context.SaveChangesAsync();

                }
                else
                {
                    await CreateRaspisanForFaculty(group?.Course?.Department?.Faculty);
                }
            }
        }


        public async Task<IActionResult> Reload()
        {


            var facs = await _context.Faculties.Include(f => f.Departments)
                .ThenInclude(ce => ce.Courses).ThenInclude(d => d.Groups).ToListAsync();

            Raspisan raspisan;
            List<string> days = new List<string>
            {
                "Душанбе",
                "Сешанбе",
                "Чоршанбе",
                "Панчшанбе",
                "Чумъа",
                "Шанбе"

            };
            Dayofweek dw;
            Timetable tm;
            Raspisangroup child;



            foreach (var f in facs)
            {
                raspisan = new Raspisan();

                if (1 < DateTime.Now.Month && DateTime.Now.Month < 7)
                {
                    raspisan.Smestr = 2;
                    raspisan.YearStart = DateTime.Now.Year - 1;
                    raspisan.YearEnd = DateTime.Now.Year;
                }
                if (8 < DateTime.Now.Month && DateTime.Now.Month < 13)
                {
                    raspisan.Smestr = 1;
                    raspisan.YearStart = DateTime.Now.Year;
                    raspisan.YearEnd = DateTime.Now.Year + 1;
                }


                foreach (var d in f.Departments)
                {

                    foreach (var c in d.Courses)
                    {
                        foreach (var g in c.Groups)
                        {
                            child = new Raspisangroup();
                            child.Group = g;

                            for (int i = 0; i < days.Count; i++)
                            {
                                dw = new Dayofweek();
                                dw.NameOfWeek = days[i];

                                for (int j = 8; j < 19; j++)
                                {
                                    tm = new Timetable();
                                    tm.Time = $"{j} - {j}:50";
                                    dw.Timetables.Add(tm);
                                }
                                child.Dayofweeks.Add(dw);
                            }

                            raspisan.Raspisangroups.Add(child);

                        }
                    }
                }

                f.Raspisans.Add(raspisan);

            }

            Sezon sezon;
            List<Group> groups = await _context.Groups.Include(g => g.Students).ToListAsync();

            foreach (var item in groups)
            {
                sezon = new Sezon();
                item.SubjectLists = null;
                if (1 < DateTime.Now.Month && DateTime.Now.Month < 7)
                {
                    sezon.Semestr = 2;
                    sezon.StartDate = DateTime.Now.Year - 1;
                    sezon.EndDate = DateTime.Now.Year;
                }
                else if (8 < DateTime.Now.Month && DateTime.Now.Month < 13)
                {
                    sezon.Semestr = 1;
                    sezon.StartDate = DateTime.Now.Year;
                    sezon.EndDate = DateTime.Now.Year + 1;
                }
                else
                {
                    return BadRequest();
                }
                item.Sezons.Add(sezon);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Main");
        }


     


    }
}