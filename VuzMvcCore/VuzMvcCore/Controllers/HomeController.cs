﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Data;
using VuzMvcCore.Models;
using VuzMvcCore.Models.ViewModels;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext context;
        UserManager<ApplicationUser> userManager;
        public HomeController(ApplicationDbContext db, UserManager<ApplicationUser> user)
        {
            userManager = user;

            this.context = db;

        }

        public async Task<IActionResult> Main()
        {
            IEnumerable<New> news = await context.News.ToListAsync();
            IEnumerable<Faculty> faculties = await context.Faculties.ToListAsync();


            MainViewModel viewModel = new MainViewModel
            {
                News = news,
                Faculties = faculties
            };

            return View(viewModel);

        }

        public IActionResult Faculties()
        {
            if (!context.Faculties.Any())
            {
                context.Faculties.Add(new Faculty()
                {
                    Name = "ФТИК",
                    NumberOfKorpus = 8,
                    DateOfOpen = new DateTime(2010, 02, 10),
                    About = "AKwdmawd sme fkm em km fe f"
                });
                context.SaveChanges();
            }
            return View(context.Faculties);
        }


        public async Task<IActionResult> Check()
        {
            if (User.Identity.IsAuthenticated)
            {
                ApplicationUser user = await userManager.FindByEmailAsync(User.Identity.Name);
                if (user != null)
                {
                    var role = await userManager.GetRolesAsync(user);
                    if (role[0] == "admin")
                        return RedirectToAction("Main", "Admin");
                    else if (role[0] == "teacher")
                        return RedirectToAction("Main", "Teacher");
                    else if (role[0] == "student")
                        return RedirectToAction("Main", "Student");
                    else
                        return RedirectToAction("Main", "Home");
                }

            }
            return RedirectToAction("", "");

        }

        public IActionResult Faculty()
        {
            return View(context.Faculties);
        }
    }
}
