﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VuzMvcCore.Data;
using VuzMvcCore.Models;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.ViewModels;
using VuzMvcCore.Models.VUZ;


namespace VuzMvcCore.Controllers
{

    public class TeacherController : Controller
    {

        ApplicationDbContext _context;
        IHostingEnvironment _enviroment;

        public TeacherController(ApplicationDbContext db, 
            IHostingEnvironment environment)
        {
            _context = db;
            _enviroment = environment;
        }


        public async Task<IActionResult> Main()
        {

            ApplicationUser user = await _context.Users
                .Include(u => u.Teacher).ThenInclude(u => u.TeacherDepartments).ThenInclude(d => d.Department)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(g => g.Group).ThenInclude(g => g.Language)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherSubjects).ThenInclude(s => s.Subject)
                .FirstOrDefaultAsync(u => u.Email == User.Identity.Name);


            if (user != null)
            {
                return View(user);
            }
            return NotFound();

        }


        [HttpGet]
        public async Task<IActionResult> ChangeData()
        {

            ChangeDataTeacherVM view = new ChangeDataTeacherVM();

            view.CurrentUser = await _context.Users
                .Include(u => u.Teacher).ThenInclude(u => u.TeacherDepartments).ThenInclude(d => d.Department)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(g => g.Group).ThenInclude(g => g.Language)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(g => g.Group).ThenInclude(g => g.Course).ThenInclude(d => d.Department)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherSubjects).ThenInclude(s => s.Subject)
                .FirstOrDefaultAsync(u => u.Email == User.Identity.Name);


            ViewData["id"] = view.CurrentUser.Id;

            view.Departments = view.CurrentUser.Teacher?.TeacherDepartments.Select(ds => ds.Department).ToList();




            foreach (var d in view.Departments)
            {
                var dep = await _context.Departments.Include(de => de.Courses).ThenInclude(c => c.Groups).FirstOrDefaultAsync(re => re.Id == d.Id);


                foreach (var c in dep.Courses)
                {
                    List<Group> g = c.Groups as List<Group>;

                    foreach (var group in g)
                    {
                        view.Groups.Add(group);
                    }
                }



            }

            view.isGroups = view.CurrentUser.Teacher.TeacherGroups.Select(g => g.Group).Select(t => t.Id).ToList();


            foreach (var d in view.Departments)
            {


                var dep = await _context.Departments.Include(de => de.SubjectDepartments)
                        .ThenInclude(se => se.Subject).FirstOrDefaultAsync(de => de.Id == d.Id);

                List<Subject> subs = dep.SubjectDepartments.Select(se => se.Subject).ToList();

                foreach (var sub in subs)
                {
                    view.Subjects.Add(sub);
                }


            }

            view.Subjects.Distinct();

            view.isSubject = view.CurrentUser.Teacher.TeacherSubjects.Select(g => g.Subject).Select(t => t.Id).ToList(); 

            return View(view);
        }


        [HttpPost]
        public async Task<IActionResult> ChangeData(ChangeDataTeacherVM view,string id, IFormFile avatar, List<Guid> groups, List<Guid> subs)
        {

            ApplicationUser user = await _context.Users.Include(u => u.Teacher)
                .ThenInclude(u => u.TeacherGroups).ThenInclude(d => d.Group).ThenInclude(g => g.Language)
                .Include(u => u.Teacher).ThenInclude(us => us.TeacherSubjects)
                .ThenInclude(t => t.Subject).FirstOrDefaultAsync(u => u.Id == id);


            if (user != null)
            {
                user.PhoneNumber = view.CurrentUser.PhoneNumber;
                user.DateOfBirth = view.CurrentUser.DateOfBirth;
                user.Name = view.CurrentUser.Name;
                user.LastName = view.CurrentUser.LastName;
                user.SurName = view.CurrentUser.SurName;

                try
                {
                    user.Age = (DateTime.Now.Year - view.CurrentUser.DateOfBirth.Value.Year);
                }
                catch (Exception)
                {
                    user.Age = 0;

                }
                user.DateOfBirth = view.CurrentUser.DateOfBirth;
                user.Teacher.Post = view.CurrentUser.Teacher.Post;
                user.Teacher.Stepen = view.CurrentUser.Teacher.Stepen;
                user.Teacher.Staj = view.CurrentUser.Teacher.Staj;
                user.Address = view.CurrentUser.Address;
                


                if (avatar != null)
                {
                    byte[] imageData = null;
                    // считываем переданный файл в массив байтов
                    using (var binaryReader = new BinaryReader(avatar.OpenReadStream()))
                    {
                        imageData = binaryReader.ReadBytes((int)avatar.Length);
                    }
                    // установка массива байтов
                    user.Avatar = imageData;
                }

                var coolGroup = user.Teacher?.TeacherGroups.Select(g => g.Group).Select(t => t.Id).ToList();


                var newGroup = groups.Except(coolGroup);

                var reGr = coolGroup.Except(groups);

                List<Group> addGroup = new List<Group>();

                List<Group> reGroup = new List<Group>();


                foreach (var item in newGroup)
                {
                    addGroup.Add(_context.Groups.FirstOrDefault(g => g.Id == item));
                }


                foreach (var item in reGr)
                {
                    reGroup.Add(_context.Groups.FirstOrDefault(g => g.Id == item));
                }


                if (addGroup.Count > 0)
                {
                    foreach (var item in addGroup)
                    {
                        user.Teacher.TeacherGroups.Add(
                            new TeacherGroup { TeacherId = user.Teacher.Id, GroupId = item.Id });
                    }
                }

                if (reGroup.Count > 0)
                {
                    foreach (var item in reGroup)
                    {
                        TeacherGroup teacherGroup = item.TeacherGroups.FirstOrDefault(s =>
                            s.TeacherId ==  user.Teacher.Id && s.GroupId == item.Id);

                        user.Teacher.TeacherGroups.Remove(teacherGroup);
                    }
                }

                var coolSub = user.Teacher?.TeacherSubjects.Select(g => g.Subject).Select(t => t.Id).ToList();


                var newSubject = subs.Except(coolSub);

                var reSubject = coolSub.Except(subs);

                List<Subject> addSub = new List<Subject>();

                List<Subject> remSub = new List<Subject>();


                foreach (var item in newSubject)
                {
                    addSub.Add(_context.Subjects.FirstOrDefault(g => g.Id == item));
                }


                foreach (var item in reSubject)
                {
                    remSub.Add(_context.Subjects.FirstOrDefault(g => g.Id == item));
                }


                if (addSub.Count > 0)
                {
                    foreach (var item in addSub)
                    {
                        user.Teacher.TeacherSubjects.Add(
                            new TeacherSubject { TeacherId = user.Teacher.Id, SubjectId = item.Id });
                    }
                }

                if (remSub.Count > 0)
                {
                    foreach (var item in remSub)
                    {
                        TeacherSubject teacherSub = item.TeacherSubjects.FirstOrDefault(s =>
                            s.TeacherId == user.Teacher.Id && s.SubjectId == item.Id);

                        user.Teacher.TeacherSubjects.Remove(teacherSub);
                    }
                }

                var res = await _context.SaveChangesAsync();

                return RedirectToAction("ChangeData", "Teacher");
            }
            

            return NotFound();
        }


        

        public async Task<IActionResult> Timestamp()
        {
            TimeTableViewNodel view = new TimeTableViewNodel();


            ApplicationUser user = await _context.Users.Include(u => u.Teacher)
                .ThenInclude(u => u.TeacherGroups).ThenInclude(d => d.Group).ThenInclude(g => g.Language)
                .Include(u => u.Teacher).ThenInclude(us => us.TeacherSubjects).ThenInclude(t => t.Subject)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherDepartments).ThenInclude(d => d.Department)
                .ThenInclude(d => d.Faculty).ThenInclude(f => f.Departments).ThenInclude(c => c.Courses).ThenInclude(d => d.Groups)
                 .Include(u => u.Teacher).ThenInclude(t => t.TeacherDepartments).ThenInclude(d => d.Department)
                .ThenInclude(d => d.Courses)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(d => d.Group)
                .ThenInclude(d => d.Course).ThenInclude(f => f.Groups).ThenInclude(g =>g.Raspisangroup)
                .Include(u => u.Teacher).ThenInclude(t => t.TeacherGroups).ThenInclude(tg => tg.Group).ThenInclude(g =>g.Raspisangroup)
                .ThenInclude(r => r.Dayofweeks).ThenInclude(d => d.Timetables)
                .FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            view.CurrentUser = user;
            
            view.Courses = user.Teacher?.TeacherGroups?.Select(t => t.Group).Select(d => d.Course).Distinct().ToList();
            view.Faculties = user.Teacher?.TeacherDepartments?.Select(t => t.Department).Select(c => c.Faculty).Distinct().ToList();
            view.Groups = user.Teacher?.TeacherGroups?.Select(t => t.Group).Distinct().ToList();
            view.Raspisangroups = user.Teacher?.TeacherGroups?.Select(t => t.Group).Select(g => g.Raspisangroup).Distinct().ToList();
            view.Subjects = user.Teacher?.TeacherSubjects?.Select(t => t.Subject).ToList();

            return View(view);
        }

        [HttpPost]
        public async Task<IActionResult> Timestamp(Guid? id, string subject, string auditoriya, Guid? groupid)
        {

            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user != null && id != null && groupid != null)
            {
                Subject sub = await _context.Subjects.FirstOrDefaultAsync(s => s.Name == subject);
                Group gr = await _context.Groups.Include(g => g.GroupSubjects).ThenInclude(gs => gs.Subject).Include(g => g.SubjectLists).FirstOrDefaultAsync(g => g.Id == groupid);

                if (gr != null)
                {

                    Timetable table = await _context.Timetables.FindAsync(id);

                    if (table != null)
                    {
                        table.Subject = subject;
                        table.NameOfTeacher = $"{user.LastName} {user.Name.Substring(0, 1).ToUpper()}. {user.SurName.Substring(0, 1).ToUpper()}";
                        table.Auditorya = auditoriya;
                        table.IsHasData = true;

                        if (!gr.SubjectLists.Select(s => s.SubjectName).Contains(sub.Name))
                        {

                            SubjectList sl = new SubjectList
                            {
                                TeacherEmail = user.Email,
                                TeacherName = table.NameOfTeacher,
                                SubjectName = sub.Name

                            };

                            gr.SubjectLists.Add(sl);
                        }

                        await _context.SaveChangesAsync();
                        return Ok();
                    }

                }

            }
               

            return NotFound();
        }


        public async Task<IActionResult> ClearRow(Guid? id)
        {
            if (id!= null)
            {
                var t = await _context.Timetables.FindAsync(id);

                if(t != null)
                {
                    t.Subject = null;
                    t.Auditorya = null;
                    t.IsHasData = false;
                    t.NameOfTeacher = null;
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Timestamp");
                }
            }
            return NotFound();
        }

        public async Task<IActionResult> TeacherTags(Guid? id)
        {

            Group gr = await _context.Groups.Include(g => g.GroupSubjects).ThenInclude(gs => gs.Subject)
                .Include(g => g.Course)
                .Include(g => g.Language)
                .Include(g => g.SubjectLists)
                .Include(g => g.Students)
                .ThenInclude(s => s.FileToTeachers)
                .FirstOrDefaultAsync(g => g.Id == id);


            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            if (gr != null )
            {
                TeacherTagsViewModel view = new TeacherTagsViewModel();
                ViewData["groupId"] = id;
                
                view.FileToGroups = await _context.FileToGroups.Where(f => f.GroupId == gr.Id && f.TeacherId == user.Teacher.Id).ToListAsync();

                view.FileToTeachers = await _context.FileToTeachers.Include(f => f.Student).ThenInclude(s => s.User).Where(f => f.GroupId == gr.Id).ToListAsync();

                view.Group = gr;
                view.SubjectLists = gr.SubjectLists.ToList();

                return View(view);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> TeacherTags(Guid? groupid, string subject, IFormFile uploadedFile)
        {
            Group gr = await _context.Groups.Include(g => g.Course).Include(g => g.Language).FirstOrDefaultAsync(g => g.Id == groupid);
            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            if (gr != null && user != null)
            {
                FileToGroup fileToGroup = new FileToGroup();

                fileToGroup.Group = gr;
                fileToGroup.Teacher = user.Teacher;

                string pathTeacher = $"{_enviroment.WebRootPath}/FileToGroup/{user.Email}";

                if (!Directory.Exists(pathTeacher))
                {
                    Directory.CreateDirectory(pathTeacher);

                }

                if (uploadedFile != null)
                {
                    string subPath = $"{pathTeacher}/{gr.Course.Number}_{gr.Title}{gr.Symbol}{gr.Language.Title.Substring(0, 1)}";

                    if (!Directory.Exists(subPath))
                    {
                        Directory.CreateDirectory(subPath);
                    }
                    // путь к папке Files
                    string path = $"{subPath}/{uploadedFile.FileName}";

                    if (!Directory.Exists(path))
                    {
                        // сохраняем файл в папку Files в каталоге wwwroot
                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        fileToGroup.PathFile = path;
                        fileToGroup.NameFile = uploadedFile.FileName;
                        fileToGroup.SubjectName = subject;
                        fileToGroup.TypeFile = uploadedFile.ContentType;
                        fileToGroup.DatePost = DateTime.Now.ToString();
                        _context.FileToGroups.Add(fileToGroup);
                        await _context.SaveChangesAsync();
                    }

                    return RedirectToAction("TeacherTags", "Teacher", new { id = gr.Id });

                }

                return BadRequest();

            }

            return NotFound();
            
        }

        public async Task<IActionResult> Raiting1(Guid? id)
        {

            Group group = await _context.Groups.Include(g => g.Students).ThenInclude(s => s.User)
                .Include(g => g.Course).Include(g => g.Language)
                .Include(g => g.SubjectLists).Include(g => g.Sezons)
                .FirstOrDefaultAsync(g => g.Id == id);


            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Subject)
                .Include(s => s.Examings).ThenInclude(r => r.Teacher)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.Student).ThenInclude(s => s.User)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.RaitingOne)
                .FirstOrDefaultAsync(s => s.Id == group.Sezons.Last().Id);
            if (group != null)
            {
                RaitingViewModel view = new RaitingViewModel();
                List<string> subs = group.SubjectLists.Where(s => s.TeacherEmail == User.Identity.Name).Select(s => s.SubjectName).ToList();
                foreach (var item in subs)
                {
                    Subject s = await _context.Subjects.FirstOrDefaultAsync(se => se.Name == item);
                    view.Subjects.Add(s);

                }
                ViewData["groupId"] = id;
                view.Students = group.Students.ToList();
                view.ReletionShips = sezon.Examings.ToList();
               
                return View(view);

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Raiting1(Guid? studentId, Guid? subjectId, Guid? groupId, int pos, int kdz, int examing)
        {
            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(t => t.Email == User.Identity.Name);
            if (studentId == null && subjectId == null)
            {
                return BadRequest();
            }
            Student student = await _context.Students.Include(u => u.User)
                .Include(s => s.Group).ThenInclude(g => g.Sezons).ThenInclude(s => s.Examings).FirstOrDefaultAsync(u => u.Id == studentId);

            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.RaitingOne)
                .FirstOrDefaultAsync(s => s.Id == student.Group.Sezons.Last().Id);

            Subject sub = await _context.Subjects.FirstOrDefaultAsync(s => s.Id == subjectId);

            Group group = await _context.Groups.Include(g => g.SubjectLists).FirstOrDefaultAsync(g => g.Id == groupId);

            if (sezon != null && student != null)
            {
                List<Examing> examings = sezon.Examings.ToList();

                Examing exam = examings.FirstOrDefault(r => r.Subject == sub);

                if (exam == null)
                {
                    exam = new Examing();
                    exam.Subject = sub;
                    exam.Teacher = user.Teacher;

                    Plase plase = new Plase();
                    plase.Student = student;
                    RaitingOne one = new RaitingOne();

                    one.Posisheniya = pos;
                    one.Kdz = kdz;
                    one.Examing = examing;
                    one.CommonBal = pos + kdz + examing;
                    plase.RaitingOne = one;
                    exam.Plases.Add(plase);
                    sezon.Examings.Add(exam);
                }
                else
                {
                    Plase plase = exam.Plases.FirstOrDefault(p => p.Student == student);

                    if (plase == null)
                    {
                        plase = new Plase();
                        plase.Student = student;

                        RaitingOne one = new RaitingOne();
                        one.Posisheniya = pos;
                        one.Kdz = kdz;
                        one.Examing = examing;
                        one.CommonBal = pos + kdz + examing;
                        plase.RaitingOne = one;
                        exam.Plases.Add(plase);
                    }
                    else
                    {
                        RaitingOne one = plase.RaitingOne;
                        if (one == null)
                        {
                            one = new RaitingOne();
                            one.Posisheniya = pos;
                            one.Kdz = kdz;
                            one.Examing = examing;
                            one.CommonBal = pos + kdz + examing;
                            plase.RaitingOne = one;
                        }
                        else
                        {
                            one.Posisheniya = pos;
                            one.Kdz = kdz;
                            one.Examing = examing;
                            one.CommonBal = pos + kdz + examing;
                        }

                    }

                }

                await _context.SaveChangesAsync();
                
                return RedirectToAction("Raiting1");
            }
            return NotFound();
        }

        public async Task<IActionResult> Raiting2(Guid? id)
        {

            Group group = await _context.Groups.Include(g => g.Students).ThenInclude(s => s.User)
                .Include(g => g.Course).Include(g => g.Language)
                .Include(g => g.SubjectLists).Include(g => g.Sezons)
                .FirstOrDefaultAsync(g => g.Id == id);


            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Subject)
                .Include(s => s.Examings).ThenInclude(r => r.Teacher)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.Student).ThenInclude(s => s.User)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.RaitingTwo)
                .FirstOrDefaultAsync(s => s.Id == group.Sezons.Last().Id);
            if (group != null)
            {
                RaitingViewModel view = new RaitingViewModel();
                List<string> subs = group.SubjectLists.Where(s => s.TeacherEmail == User.Identity.Name).Select(s => s.SubjectName).ToList();
                foreach (var item in subs)
                {
                    Subject s = await _context.Subjects.FirstOrDefaultAsync(se => se.Name == item);
                    view.Subjects.Add(s);

                }
                ViewData["groupId"] = id;
                view.Students = group.Students.ToList();
                view.ReletionShips = sezon.Examings.ToList();

                return View(view);

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Raiting2(Guid? studentId, Guid? subjectId, Guid? groupId, int pos, int kdz, int examing)
        {
            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(t => t.Email == User.Identity.Name);
            if (studentId == null && subjectId == null)
            {
                return BadRequest();
            }
            Student student = await _context.Students.Include(u => u.User)
                .Include(s => s.Group).ThenInclude(g => g.Sezons).ThenInclude(s => s.Examings).FirstOrDefaultAsync(u => u.Id == studentId);

            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.RaitingTwo)
                .FirstOrDefaultAsync(s => s.Id == student.Group.Sezons.Last().Id);

            Subject sub = await _context.Subjects.FirstOrDefaultAsync(s => s.Id == subjectId);

            Group group = await _context.Groups.Include(g => g.SubjectLists).FirstOrDefaultAsync(g => g.Id == groupId);

            if (sezon != null && student != null)
            {
                List<Examing> examings = sezon.Examings.ToList();

                Examing exam = examings.FirstOrDefault(r => r.Subject == sub);

                if (exam == null)
                {
                    exam = new Examing();
                    exam.Subject = sub;
                    exam.Teacher = user.Teacher;

                    Plase plase = new Plase();
                    plase.Student = student;
                    RaitingTwo two = new RaitingTwo();

                    two.Posisheniya = pos;
                    two.Kdz = kdz;
                    two.Examing = examing;
                    two.CommonBal = pos + kdz + examing;
                    plase.RaitingTwo = two;
                    exam.Plases.Add(plase);
                    sezon.Examings.Add(exam);
                }
                else
                {
                    Plase plase = exam.Plases.FirstOrDefault(p => p.Student == student);

                    if (plase == null)
                    {
                        plase = new Plase();
                        plase.Student = student;

                        RaitingTwo two = new RaitingTwo();
                        two.Posisheniya = pos;
                        two.Kdz = kdz;
                        two.Examing = examing;
                        two.CommonBal = pos + kdz + examing;
                        plase.RaitingTwo = two;
                        exam.Plases.Add(plase);
                    }
                    else
                    {
                        RaitingTwo two = plase.RaitingTwo;
                        if (two == null)
                        {
                            two = new RaitingTwo();
                            two.Posisheniya = pos;
                            two.Kdz = kdz;
                            two.Examing = examing;
                            two.CommonBal = pos + kdz + examing;
                            plase.RaitingTwo = two;
                        }
                        else
                        {
                            two.Posisheniya = pos;
                            two.Kdz = kdz;
                            two.Examing = examing;
                            two.CommonBal = pos + kdz + examing;
                        }

                    }

                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Raiting2");
            }
            return NotFound();
        }



        public async Task<IActionResult> Session(Guid? id)
        {

            Group group = await _context.Groups.Include(g => g.Students).ThenInclude(s => s.User)
                .Include(g => g.Course).Include(g => g.Language)
                .Include(g => g.SubjectLists).Include(g => g.Sezons)
                .FirstOrDefaultAsync(g => g.Id == id);


            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Subject)
                .Include(s => s.Examings).ThenInclude(r => r.Teacher)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.Student).ThenInclude(s => s.User)
                .Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.Sessions)
                .FirstOrDefaultAsync(s => s.Id == group.Sezons.Last().Id);
            if (group != null)
            {
                RaitingViewModel view = new RaitingViewModel();
                List<string> subs = group.SubjectLists.Where(s => s.TeacherEmail == User.Identity.Name).Select(s => s.SubjectName).ToList();
                foreach (var item in subs)
                {
                    Subject s = await _context.Subjects.FirstOrDefaultAsync(se => se.Name == item);
                    view.Subjects.Add(s);

                }
                ViewData["groupId"] = id;
                view.Students = group.Students.ToList();
                view.ReletionShips = sezon.Examings.ToList();

                return View(view);

            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Session(Guid? studentId, Guid? subjectId, Guid? groupId, int bal )
        {
            ApplicationUser user = await _context.Users.Include(u => u.Teacher).FirstOrDefaultAsync(t => t.Email == User.Identity.Name);
            if (studentId == null && subjectId == null)
            {
                return BadRequest();
            }
            Student student = await _context.Students.Include(u => u.User)
                .Include(s => s.Group).ThenInclude(g => g.Sezons).ThenInclude(s => s.Examings).FirstOrDefaultAsync(u => u.Id == studentId);

            Sezon sezon = await _context.Sezons.Include(s => s.Examings).ThenInclude(r => r.Plases).ThenInclude(e => e.Sessions)
                .FirstOrDefaultAsync(s => s.Id == student.Group.Sezons.Last().Id);

            Subject sub = await _context.Subjects.FirstOrDefaultAsync(s => s.Id == subjectId);

            Group group = await _context.Groups.Include(g => g.SubjectLists).FirstOrDefaultAsync(g => g.Id == groupId);

            if (sezon != null && student != null)
            {
                List<Examing> examings = sezon.Examings.ToList();

                Examing exam = examings.FirstOrDefault(r => r.Subject == sub);

                if (exam == null)
                {
                    exam = new Examing();
                    exam.Subject = sub;
                    exam.Teacher = user.Teacher;

                    Plase plase = new Plase();
                    plase.Student = student;
                    Session session = new Session();

                    session.Bal = bal;
                    plase.Sessions = session;
                    exam.Plases.Add(plase);
                    sezon.Examings.Add(exam);
                }
                else
                {
                    Plase plase = exam.Plases.FirstOrDefault(p => p.Student == student);

                    if (plase == null)
                    {
                        plase = new Plase();
                        plase.Student = student;

                        Session session = new Session();

                        session.Bal = bal;

                        plase.Sessions = session;
                        exam.Plases.Add(plase);
                    }
                    else
                    {
                        Session session = plase.Sessions;
                        if (session == null)
                        {
                            session = new Session();
                            session.Bal = bal;
                            plase.Sessions = session;
                        }
                        else
                        {
                            session.Bal = bal;
                        }

                    }

                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Session");
            }
            return NotFound();
        }



        public async Task<IActionResult> Result(Guid? id)
        {

            Group gr = await _context.Groups.Include(g => g.Students).ThenInclude(s => s.User)
                .Include(g => g.Course).Include(g => g.Language)
                .Include(g => g.Sezons).ThenInclude(s => s.Examings).ThenInclude(e => e.Subject)
                .Include(g => g.Sezons).ThenInclude(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.Student)
                .Include(g => g.Sezons).ThenInclude(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.FinalyBals)
                .FirstOrDefaultAsync(g => g.Id == id);
            
            if (gr != null)
            {
                ViewData["sezonId"] = gr.Sezons.Last().Id;
                ViewData["groupId"] = gr.Id;
                List<Examing> examings = gr.Sezons.Last().Examings.ToList();
                return View(examings);
            }
            return NotFound();

        }

        [HttpPost]
        public async Task<IActionResult> Resultat(Guid? sezonId)
        {
            Sezon sezon = await _context.Sezons
                .Include(s => s.Group)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.FinalyBals)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.RaitingOne)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.RaitingTwo)
                .Include(s => s.Examings).ThenInclude(e => e.Plases).ThenInclude(p => p.Sessions)
                .FirstOrDefaultAsync(s => s.Id == sezonId);

            if (sezon != null )
            {

                double pk1 = 0;
                double pk2 = 0;
                double session = 0;

                List<Examing> examings = sezon.Examings.ToList();

                foreach (var e in examings)
                {
                    foreach (var p in e.Plases)
                    {

                        pk1 = p.RaitingOne != null ? p.RaitingOne.CommonBal : 0;
                        pk2 = p.RaitingTwo != null ? p.RaitingTwo.CommonBal : 0;
                        session = p.Sessions != null ? p.Sessions.Bal : 0;

                        if (p.FinalyBals == null)
                        {
                            FinalyBal finaly = new FinalyBal();
                            finaly.Finaly = Bal(pk1, pk2, session);
                            finaly.Baxo = Baxo(finaly.Finaly);
                            p.FinalyBals = finaly;
                        }
                        else
                        {
                            p.FinalyBals.Finaly = Bal(pk1, pk2, session);
                            p.FinalyBals.Baxo = Baxo(p.FinalyBals.Finaly);
                        }
                        
                    }
                }



                await _context.SaveChangesAsync();

                return RedirectToAction("Result", "Teacher", new { id = sezon.Group.Id});
            }
            return NotFound();
        }


        public double Bal(double pk1, double pk2, double session)
        {
            double res;

            res = (((pk1 + pk2) / 2) * 0.6) + (session * 0.4);

            res = Math.Round(res, 1);
            return res;
        }


        public string Baxo(double bal)
        {
            double temp = Math.Ceiling(bal);
            string str;
            if (temp < 50)
            {
                str = "F";
            }
            else if (temp < 55)
            {
                str = "D-";
            }
            else if (temp < 60)
            {
                str = "D+";
            }
            else if (temp < 65)
            {
                str = "C-";
            }
            else if (temp < 70)
            {
                str = "C";
            }
            else if (temp < 75)
            {
                str = "C+";
            }
            else if (temp < 80)
            {
                str = "B-";
            }
            else if (temp < 85)
            {
                str = "B";
            }
            else if (temp < 90)
            {
                str = "B+";
            }
            else if (temp < 95)
            {
                str = "A-";
            }
            else if (temp <= 100)
            {
                str = "A";
            }
            else
            {
                str = "";
            }
            return str;



        }

        [HttpPost]
        public IActionResult GetFile(string path, string name, string type)
        {
            return PhysicalFile(path, type, name);
        }
    }
}