﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class RaitingOne
    {
        public Guid Id { get; set; }
        public Guid PlaseId { get; set; }
        public Plase Plase { get; set; }
        public int Posisheniya { get; set; }
        public int Kdz { get; set; }
        public int Examing { get; set; }
        public int CommonBal { get; set; }
        
    }
}
