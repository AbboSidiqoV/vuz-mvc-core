﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class Plase
    {
        public Guid Id{ get; set; }

        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public Guid ExamingId { get; set; }
        public Examing Examing { get; set; }

        public RaitingOne RaitingOne { get; set; }

        public RaitingTwo RaitingTwo { get; set; }

        public Session Sessions { get; set; }

        public FinalyBal FinalyBals { get; set; }

    }
}
