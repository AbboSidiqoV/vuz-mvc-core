﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Timetable
    {
        public Guid Id { get; set; }
        //Время
        public string Time { get; set; }
        //Внешный ключь DayOfWeekId
        public Guid DayOfWeekId { get; set; }
        public Dayofweek Dayofweek { get; set; }
        //Предметь 
        public string Subject { get; set; }
        //Аудитория
        public string Auditorya { get; set; }
        //Имя преподавателя
        public string NameOfTeacher { get; set; }
        //Проверяет заполнения поля
        public bool IsHasData { get; set; }
    }
}
