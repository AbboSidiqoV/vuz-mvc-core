﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Relations;

namespace VuzMvcCore.Models.VUZ
{
    public class Subject
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<TeacherSubject> TeacherSubjects { get; set; }

        public ICollection<SubjectDepartment> SubjectDepartments { get; set; }


        public ICollection<GroupSubject> GroupSubjects { get; set; }

        public Examing Examing { get; set; }


        public Subject()
        {
            SubjectDepartments = new List<SubjectDepartment>();
            TeacherSubjects = new List<TeacherSubject>();
            GroupSubjects = new List<GroupSubject>();
        }
    }
}
