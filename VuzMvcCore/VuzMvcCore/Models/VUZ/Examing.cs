﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class Examing
    {
        public Guid Id { get; set; }

        public Guid SezonId { get; set; }
        public Sezon Sezon { get; set; }

        public Guid? SubjectId { get; set; }
        public Subject Subject { get; set; }


        public Guid? TeacherId { get; set; }
        public Teacher Teacher { get; set; }


        public ICollection<Plase> Plases { get; set; }

        public Examing()
        {
            Plases = new List<Plase>();
        }
    }
}
