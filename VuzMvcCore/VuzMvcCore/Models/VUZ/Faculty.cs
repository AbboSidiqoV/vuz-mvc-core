﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Faculty
    {
        public Guid Id { get; set; }

        [Display(Name = "Имя")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Номер корпуса")]
        [Required]
        public int NumberOfKorpus { get; set; }


        [Display(Name = "Дата основания")]
        public DateTime? DateOfOpen { get; set; }

        [Display(Name = "О факультет")]
        public string About { get; set; }

        public ICollection<Raspisan> Raspisans { get; set; }

        public ICollection<Department> Departments { get; set; }

        public Faculty()
        {

            Departments = new List<Department>();
            Raspisans = new List<Raspisan>();
        }
    }
}
