﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Dayofweek
    {
        public Guid Id { get; set; }
        //Названия день недели
        public string NameOfWeek { get; set; }
        //Внешный ключь RaspisangroupId
        public Guid RaspisangroupId { get; set; }
        public Raspisangroup Raspisangroup { get; set; }
        //Связ межжду Timetables(один ко многим)
        public ICollection<Timetable> Timetables { get; set; }
        public Dayofweek()
        {
            Timetables = new List<Timetable>();
        }
    }
}
