﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Raspisangroup
    {
        [Key]
        public Guid Id { get; set; }
        //Внешный ключь RaspisanId
        public Guid RaspisanId { get; set; }
        public Raspisan Raspisan { get; set; }
        //Внешный ключь GroupId
        public Guid GroupId { get; set; }
        public Group Group { get; set; }
        //Связ межжду Dayofweeks(один ко многим)
        public ICollection<Dayofweek> Dayofweeks { get; set; }
    }
}
