﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class Group
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Символ")]
        public string Symbol { get; set; }


        public int? LanguageId { get; set; }

        public Language Language { get; set; }

        public Guid CourseId { get; set; }

        public Course Course { get; set; }

        public ICollection<FileToGroup> FileToGroups { get; set; }

        public ICollection<Student> Students { get; set; }

        public ICollection<TeacherGroup> TeacherGroups { get; set; }

        public Raspisangroup Raspisangroup { get; set; }

        public ICollection<GroupSubject> GroupSubjects { get; set; }


        public ICollection<FileToTeacher> FileToTeachers { get; set; }
        public ICollection<SubjectList> SubjectLists { get; set; }

        public ICollection<Sezon> Sezons { get; set; }
        public Group()
        {
            Students = new List<Student>();
            TeacherGroups = new List<TeacherGroup>();
            GroupSubjects = new List<GroupSubject>();
            FileToGroups = new List<FileToGroup>();
            SubjectLists = new List<SubjectList>();
            FileToTeachers = new List<FileToTeacher>();

            Sezons = new List<Sezon>();
        }
    }
}
