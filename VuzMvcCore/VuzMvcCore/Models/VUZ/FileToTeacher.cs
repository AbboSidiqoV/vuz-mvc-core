﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class FileToTeacher
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public Student Student { get; set; }

        public Guid GroupId { get; set; }
        public Group Group { get; set; }
        public string TypeFile { get; set; }
        public string NameFile { get; set; }
        public string PathFile { get; set; }

        public string SubjectName { get; set; }

        public string DatePost { get; set; }



    }
}
