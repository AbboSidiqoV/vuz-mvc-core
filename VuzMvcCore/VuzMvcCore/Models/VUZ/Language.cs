﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Language
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public ICollection<Group> Groups { get; set; }

        public Language()
        {
            Groups = new List<Group>();
        }
    }
}
