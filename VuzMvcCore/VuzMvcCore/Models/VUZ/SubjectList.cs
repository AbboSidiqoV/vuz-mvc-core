﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class SubjectList
    {
        public Guid Id { get; set; }

        public Guid GroupId { get; set; }

        public Group Group { get; set; }

        public string TeacherName { get; set; }
        public string TeacherEmail { get; set; }
        public string SubjectName { get; set; }
    }
}





















