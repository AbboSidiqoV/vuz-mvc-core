﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Raspisan
    {
        public Guid Id { get; set; }
        //Начвло года
        public int YearStart { get; set; }
        //Год оканчания
        public int YearEnd { get; set; }
        //Семестр
        public int Smestr { get; set; }
        //Внешный ключь
        public Guid? FacultyId { get; set; }
        public Faculty Faculty { get; set; }
        //Связ межжду Raspisangroups(один ко многим)
        public ICollection<Raspisangroup> Raspisangroups { get; set; }

        public Raspisan()
        {
            Raspisangroups = new List<Raspisangroup>();
        }
    }
}
