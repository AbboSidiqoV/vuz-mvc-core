﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class FinalyBal
    {
        public Guid Id { get; set; }
        public Guid PlaseId { get; set; }
        public Plase Plase { get; set; }
        public double Finaly { get; set; }
        public string Baxo { get; set; }
    }
}
