﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Relations;

namespace VuzMvcCore.Models.VUZ
{
    public class Department
    {

        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }


        public string About { get; set; }


        public DateTime? DateOfOpen { get; set; }

        public Guid FacultyId { get; set; }
        public Faculty Faculty { get; set; }

        public IEnumerable<Course> Courses { get; set; }

        //Многим ко многим
        public ICollection<TeacherDepartment> TeacherDepartments { get; set; }
        public ICollection<SubjectDepartment> SubjectDepartments { get; set; }


        public Department()
        {
            TeacherDepartments = new List<TeacherDepartment>();
            Courses = new List<Course>();
            SubjectDepartments = new List<SubjectDepartment>();

        }
    }
}
