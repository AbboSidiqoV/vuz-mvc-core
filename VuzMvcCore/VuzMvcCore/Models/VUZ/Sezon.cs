﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models.VUZ
{
    public class Sezon
    {

        public Guid Id { get; set; }

        public int StartDate { get; set; }

        public int EndDate { get; set; }

        public int Semestr { get; set; }

        public Guid? GroupId { get; set; }

        public Group Group { get; set; }

        public ICollection<Examing> Examings { get; set; }

        public Sezon()
        {
            Examings = new List<Examing>();
        }
    }
}
