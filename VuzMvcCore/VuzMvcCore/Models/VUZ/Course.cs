﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.VUZ
{
    public class Course
    {
        public Guid Id { get; set; }
        //Номер курса
        public int Number { get; set; }
        //Внешный ключь DepartmentId
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
        //Связь между Groups(один ко многим)
        public ICollection<Group> Groups { get; set; }

        public Course()
        {
            Groups = new List<Group>();
        }
    }
}
