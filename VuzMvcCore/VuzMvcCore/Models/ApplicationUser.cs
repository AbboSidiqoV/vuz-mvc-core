﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;

namespace VuzMvcCore.Models
{
    public class ApplicationUser : IdentityUser
    {

        public  string Fio { get; set; }


        [Display(Name = "Имя")]
        public  string Name { get; set; }

        [Display(Name = "Отечество")]
        public  string SurName { get; set; }


        [Display(Name = "Фамиля")]
        public  string LastName { get; set; }


        [Display(Name = "Дата рождения")]
        public DateTime? DateOfBirth { get; set; }



        public byte[] Avatar { get; set; }


        [Display(Name = "Возрасть")]
        public int Age { get; set; }


        [Display(Name = "Адрес")]
        public  string Address { get; set; }

        public bool IsSignIn { get; set; }

        public Teacher Teacher { get; set; }

        public Student Student { get; set; }


    }
}
