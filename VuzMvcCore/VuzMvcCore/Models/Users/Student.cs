﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Users
{
    public class Student
    {
        public Guid Id { get; set; }
        public int StartDate { get; set; }
        public int EndDate { get; set; } 
        public string TypeStudy { get; set; }
        public string Study { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string Stipendy { get; set; }
        public Guid GruopId { get; set; }
        public Group Group { get; set; }
  
        public ICollection<FileToTeacher> FileToTeachers { get; set; }

        public ICollection<Plase> Plases  { get; set; }
        public Student()
        {
            FileToTeachers = new List<FileToTeacher>();
            Plases = new List<Plase>();
        }

    }
}
