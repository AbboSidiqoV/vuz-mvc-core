﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Users
{
    public class Teacher
    {
        public Guid Id { get; set; }


        [Display(Name = "Стаж")]
        public int Staj { get; set; }


        public Guid? WorkerId { get; set; }


        [Display(Name = "Дата регистрация")]
        public DateTime DateOfStartWork { get; set; }


        [Display(Name = "Должность")]
        public string Post { get; set; }


        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Display(Name = "Cтепень наименование")]
        public string Stepen { get; set; }

        public ICollection<FileToGroup> FileToGroups { get; set; }

        //связь многим ко многим
        public ICollection<TeacherSubject> TeacherSubjects { get; set; }

        //связь многим ко многим
        public ICollection<TeacherDepartment> TeacherDepartments { get; set; }
        public ICollection<TeacherGroup> TeacherGroups { get; set; }
        
        public ICollection<Examing> Examings { get; set; }

        public Teacher()
        {
            TeacherSubjects = new List<TeacherSubject>();

            TeacherDepartments = new List<TeacherDepartment>();

            FileToGroups = new List<FileToGroup>();

            TeacherGroups = new List<TeacherGroup>();

            Examings = new List<Examing>();
            
        }
    }
}
