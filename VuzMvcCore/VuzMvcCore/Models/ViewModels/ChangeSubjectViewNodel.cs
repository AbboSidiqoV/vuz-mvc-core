﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class ChangeSubjectViewNodel
    {
        public List<string> DepartSubjects { get; set; }

        public List<Subject> Subjects { get; set; }

        public ChangeSubjectViewNodel()
        {
            DepartSubjects = new List<string>();
            Subjects = new List<Subject>();
        }
    }
}
