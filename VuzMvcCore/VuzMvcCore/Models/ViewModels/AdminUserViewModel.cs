﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models.ViewModels
{
    public class AdminUserViewModel
    {

        public IEnumerable<ApplicationUser> ApplicationUsers { get; set; }

        public Filter Filter { get; set; }


    }
}
