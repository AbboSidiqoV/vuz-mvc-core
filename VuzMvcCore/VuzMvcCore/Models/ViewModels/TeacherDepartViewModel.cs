﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class TeacherDepartViewModel
    {
        public List<string> TeacherDeparts { get; set; }

        public List<Department> Departments { get; set; }
        public List<Faculty> Faculties { get; internal set; }

        public TeacherDepartViewModel()
        {
            TeacherDeparts = new List<string>();
            Departments = new List<Department>();
            Faculties = new List<Faculty>();
        }
    }
}
