﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class ChangeDataTeacherVM
    {
        public ApplicationUser CurrentUser { get; set; }

        public List<Subject> Subjects { get; set; }


        public List<Department> Departments { get; set; }
        public List<Guid> isSubject { get; set; }

        public List<Group> Groups { get; set; }

        public List<Guid> isGroups { get; set; }

        public ChangeDataTeacherVM()
        {
            Subjects = new List<Subject>();

            isSubject = new List<Guid>();

            Groups = new List<Group>();
            isGroups = new List<Guid>();


        }
    }
}
