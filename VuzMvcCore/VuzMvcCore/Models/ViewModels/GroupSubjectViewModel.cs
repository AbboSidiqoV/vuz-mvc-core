﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Relations;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class GroupSubjectViewModel
    {
        public Group Group { get; set; }

        public List<SubjectList> SubjectLists { get; set; }

        public List<FileToGroup> FileToGroups { get; set; }

        public GroupSubjectViewModel()
        {
            FileToGroups = new List<FileToGroup>();
            SubjectLists = new List<SubjectList>();
        }
    }
}
