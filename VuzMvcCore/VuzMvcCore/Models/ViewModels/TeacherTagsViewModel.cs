﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class TeacherTagsViewModel
    {
        public ApplicationUser CurrUser { get; set; }

        public List<FileToGroup> FileToGroups { get; set; }
        public List<FileToTeacher> FileToTeachers { get; set; }


        public Group Group { get; set; }
        public List<SubjectList> SubjectLists { get; set; }

        public TeacherTagsViewModel()
        {
            FileToGroups = new List<FileToGroup>();
            FileToTeachers = new List<FileToTeacher>();
            CurrUser = new ApplicationUser();
            SubjectLists = new List<SubjectList>();
        }
    }
}
