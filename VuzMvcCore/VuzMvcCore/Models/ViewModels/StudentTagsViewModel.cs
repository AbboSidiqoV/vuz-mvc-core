﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class StudentTagsViewModel
    {
        public ApplicationUser CurrUser { get; set; }

        public List<FileToGroup> FileToGroups { get; set; }
        public List<FileToTeacher> FileToTeachers { get; set; }

        public SubjectList SubjectList { get; set; }

        public StudentTagsViewModel()
        {
            FileToGroups = new List<FileToGroup>();
            FileToTeachers = new List<FileToTeacher>();
            CurrUser = new ApplicationUser();
            SubjectList = new SubjectList();
        }
    }
}
