﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class StudentExamingViewModel
    {
        public Examing  Examing{ get; set; }
        public Sezon  Sezon{ get; set; }

        public ApplicationUser User { get; set; }
    }
}
