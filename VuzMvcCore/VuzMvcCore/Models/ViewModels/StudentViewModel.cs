﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class StudentViewModel
    {
        public List<Faculty> Faculties { get; set; }

        public List<string> Courses { get; set; }
        public List<Department> Departments { get; set; }
        public List<Group> Groups { get; set; }

        public string FacName { get; set; }
        public string DepName { get; set; }
        public string CourseName { get; set; }
        public string GroupName { get; set; }


        public StudentViewModel()
        {
            Faculties = new List<Faculty>();
            Courses = new List<string>();
            Departments = new List<Department>();
            Groups = new List<Group>();
        }


    }
}
