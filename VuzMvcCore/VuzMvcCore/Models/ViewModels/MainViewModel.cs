﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class MainViewModel
    {
        public IEnumerable<Faculty> Faculties { get; set; }

        public IEnumerable<New> News { get; set; }
    }
}
