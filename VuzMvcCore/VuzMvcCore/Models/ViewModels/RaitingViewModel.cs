﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class RaitingViewModel
    {
        public List<Subject> Subjects { get; set; }

        public List<Student> Students { get; set; }

        public List<Examing> ReletionShips { get; set; }

        public Sezon Sezon { get;  set; }

        public RaitingViewModel()
        {
            Subjects = new List<Subject>();

            Students = new List<Student>();
            ReletionShips = new List<Examing>();
        }
    }
}
