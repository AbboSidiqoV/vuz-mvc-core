﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.ViewModels
{
    public class TimeTableViewNodel
    {
        public List<Raspisangroup> Raspisangroups { get; set; }

        public ApplicationUser CurrentUser { get; set; }
        public List<Course> Courses { get;  set; }
        public List<Faculty> Faculties { get;  set; }
        public List<Group> Groups { get;  set; }
        public List<Subject> Subjects { get;  set; }

        public Timetable Timetable { get; set; }
        public Dayofweek Dayofweek { get; set; }

        public TimeTableViewNodel()
        {
            Raspisangroups = new List<Raspisangroup>();
            CurrentUser = new ApplicationUser();
            Courses = new List<Course>();
            Faculties = new List<Faculty>();
            Groups = new List<Group>();
            Subjects = new List<Subject>();
            Timetable = new Timetable();
            Dayofweek = new Dayofweek();
        }
    }
}
