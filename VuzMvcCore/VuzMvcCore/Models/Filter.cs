﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuzMvcCore.Models
{
    public class Filter
    {
        public string Active { get; set; }

        public string Role { get; set; }
    }
}
