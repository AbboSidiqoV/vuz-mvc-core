﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Relations
{
    public class TeacherSubject
    {
        public Guid? TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public Guid? SubjectId { get; set; }

        public Subject Subject { get; set; }
    }
}
