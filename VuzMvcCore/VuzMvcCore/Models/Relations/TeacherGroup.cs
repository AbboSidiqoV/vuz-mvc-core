﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Relations
{
    public class TeacherGroup
    {
        public Guid Id { get; set; }
        public Guid TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public Guid GroupId { get; set; }
        public Group Group { get; set; }
    }
}
