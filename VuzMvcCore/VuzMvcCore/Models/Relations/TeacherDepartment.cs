﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.Users;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Relations
{
    public class TeacherDepartment
    {
        public Guid? TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public Guid? DepartmentId { get; set; }

        public Department Department { get; set; }
    }
}
