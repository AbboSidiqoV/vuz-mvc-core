﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Relations
{
    public class SubjectDepartment
    {
        public Guid? SubjectId { get; set; }

        public Subject Subject { get; set; }

        public Guid? DepartmentId { get; set; }

        public Department Department { get; set; }
    }
}
