﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuzMvcCore.Models.VUZ;

namespace VuzMvcCore.Models.Relations
{
    public class GroupSubject
    {
        public Guid Id { get; set; }
        public Guid? SubjectId { get; set; }

        public Subject Subject { get; set; }

        public Guid? GroupId { get; set; }
        public Group Group { get; set; }
    }
}
